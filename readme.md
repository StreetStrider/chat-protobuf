# chat-protobuf
## dev
```sh
npm start
# <new terminal>
cd web/; npm start
```
`dev` release can be found in `release/dev/`.

## release
```
npm run release
```
Then, `battle` release can be found in `release/battle/`.

## live
http://46.101.225.245/chat/
