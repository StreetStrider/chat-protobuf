
import Auth from 'Auth/Auth'
import Chat from 'Chat/Chat'

export default
[
	{ path: '/', redirect: '/auth' },

	{ path: '/auth', component: Auth },
	{ name: 'chat', path: '/chat', component: Chat, props: true },
]
