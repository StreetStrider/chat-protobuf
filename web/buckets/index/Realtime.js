/* @flow */
/* global io */
/* :: declare var io: Function */

export default function Realtime ()
{
	return io('ws://' + location.host,
	{
		path: location.pathname + 'socket.io/',
	})
}
