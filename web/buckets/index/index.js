/* @flow */

// Vue
// import Vue from 'vue'
/* @flow-off */
import Vue from 'vue/dist/vue.esm.js'
/* @flow-off */
import Router from 'vue-router'

Vue.config.devtools = false
Vue.config.productionTip = false

Vue.use(Router)


// App
import routes from './routes'
import Realtime from './Realtime'

document.addEventListener('DOMContentLoaded', () =>
{
	var Chat = new Vue(
	{
		router: new Router({ routes }),

		provide:
		{
			realtime: Realtime(),
		},

		data ()
		{
			var data =
			{
				alias: null,
			}

			return data
		}
	})

	Chat.$mount('#app')
})
