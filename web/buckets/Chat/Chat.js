/* @flow */
/* global protochat */
/* :: declare var protochat: any */

import find from 'lodash-es/findIndex'

/* @flow-off */
import template from './Chat.static.pug'

export default
{
	template,

	inject: [ 'realtime' ],

	beforeRouteEnter: function (
		to   /* :any */,
		from /* :any */,
		next /* :Function */
	)
	{
		var alias = to.params.alias

		if (alias)
		{
			next()
		}
		else
		{
			next('/auth')
		}
	},

	mounted ()
	{
		this.join(this.alias)

		this.realtime.on('joined', this.joined)
		this.realtime.on('leaved', this.leaved)
		this.realtime.on('talkee-list', this.talkee_list)
		this.realtime.on('recv', this.recv)

		window.onbeforeunload = this.leave
	},

	destroyed ()
	{
		this.leave()
		window.onbeforeunload = ''

		this.realtime.off('joined', this.joined)
		this.realtime.off('leaved', this.leaved)
		this.realtime.off('talkee-list', this.talkee_list)
		this.realtime.off('recv', this.recv)
	},

	props:
	{
		alias:
		{
			type: String,
			required: true,
		},
	},

	data ()
	{
		var data =
		{
			msg: '',

			replies:
			[
				// { talkee: { alias, id }, msg },
				// { special, ... }
			],

			talkee:
			[
				// { alias, id },
			],
		}

		return data
	},

	methods:
	{
		/* send/recv */
		send ()
		{
			var msg = this.msg.trim()

			if (msg)
			{
				this.msg = ''

				this.replies.push(
				{
					talkee: { alias: this.alias },
					msg: msg,
				})

				this.realtime.emit('send', { msg })
			}
		},

		recv: function (data /* :Object */)
		{
			data = proto_decode(protochat.Reply, data)

			this.replies.push(data)
		},


		/* join/leave */
		join: function (alias /* :string */)
		{
			var alias = proto_encode(protochat.Alias, { alias })
			this.realtime.emit('join', alias)
		},

		leave ()
		{
			this.realtime.emit('leave')
		},


		/* joined/leaved */
		joined: function (data /* :Object */)
		{
			data = proto_decode(protochat.Talkee, data)

			this.talkee.push(data)
			this.replies.push({ special: 'joined', alias: data.alias })
		},

		leaved: function (data /* :Object */)
		{
			data = proto_decode(protochat.Talkee, data)

			var index = find(this.talkee, { id: data.id })

			if (~ index)
			{
				this.talkee.splice(index, 1)
				this.replies.push({ special: 'leaved', alias: data.alias })
			}
		},


		/* list */
		talkee_list: function (data /* :Object */)
		{
			data = proto_decode(protochat.TalkeeList, data)

			this.talkee = data.talkeeList
		},
	}
}

function proto_decode (Type, payload)
{
	payload = new Uint8Array(payload)
	payload = Type.decode(payload)
	return payload
}

function proto_encode (Type, payload)
{
	payload = Type.encode(Type.create(payload)).finish()
	payload = payload.buffer.slice(0, payload.length)
	return payload
}
