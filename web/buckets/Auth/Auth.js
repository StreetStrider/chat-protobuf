
import template from './Auth.static.pug'

export default
{
	template,

	data ()
	{
		var data =
		{
			alias: 'some-name',
		}

		return data
	},

	methods:
	{
		login ()
		{
			var alias = this.alias.trim()

			if (alias)
			{
				this.$router.push({ name: 'chat', params: { alias }})
			}
		}
	},
}
