/* @flow */

type Protobuf$Type =
{
	create (payload: Object): any,
	encode (payload: any): any,
	decode (payload: Buffer): Object,
}

import protobufjs from 'protobufjs'

export default function ProtoChat (app: Object)
{
	var protochat = {}

	protochat.ready = new Promise((rs, rj) =>
	{
		protobufjs.load(app.root('proto/chat.json'), (err, proto) =>
		{
			if (err)
			{
				return rj(err)
			}

			rs(proto)
			protochat.proto = proto
		})
	})

	protochat.encode = (Type: Protobuf$Type, payload: Object): Buffer =>
	{
		return Type.encode(Type.create(payload)).finish()
	}

	protochat.decode = (Type: Protobuf$Type, payload: Buffer): Object =>
	{
		return Type.decode(payload)
	}

	return protochat
}
