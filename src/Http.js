
import Express from 'express'

export default function (app)
{
	var port = 8081

	var http = {}

	http.express = Express()

	http.express.use(Express.static(app.root('web/')))

	http.ready = new Promise(rs =>
	{
		http.server = http.express.listen(port, rs)
	})
	.then(() =>
	{
		console.info(`HTTP at :${ port }`)
	})

	return http
}
