/* @flow */

import Rootpath from 'rootpath'

import Http from './Http'
import Realtime from './Realtime'
import Chat from './Chat'
import ProtoChat from './ProtoChat'

export default function ()
{
	var app = {}

	app.root = Rootpath(__dirname, '..')
	console.info('package at `%s`', app.root())

	app.http = Http(app)
	app.realtime = Realtime(app.http.server)
	app.protochat = ProtoChat(app)

	app.ready = Promise.all([ app.http.ready, app.protochat.ready ])
	app.ready.catch(console.error)

	app.ready.then(() =>
	{
		app.chat = Chat(app)
	})

	return app
}
