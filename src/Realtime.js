/* @flow */
/* global net$Server */

import Socketio from 'socket.io'

export default function Realtime (http: net$Server)
{
	return Socketio().attach(http)
}
