/* @flow */

import uuid from 'uuid/v1'

import find from 'lodash/findIndex'
import pick from 'lodash/pick'

export default function Chat (app: Object)
{
	var realtime = app.realtime

	var protochat = app.protochat
	var proto = protochat.proto

	function talkee_public (data)
	{
		return pick(data, [ 'id', 'alias' ])
	}

	var Aliases = []

	/* @flow-off */
	Aliases.public = () =>
	{
		return Aliases.map(it => talkee_public(it))
	}

	realtime.on('connection', socket =>
	{
		socket.on('join', (data) =>
		{
			data = protochat.decode(proto.Alias, data)

			var id = uuid()
			var alias = data.alias

			socket.chat =
			{
				id,
				alias,
			}

			Aliases.push(socket.chat)

			var talkee = talkee_public(socket.chat)
			socket.broadcast.emit('joined', protochat.encode(proto.Talkee, talkee))

			/* @flow-off */
			var talkee_list = { talkeeList: Aliases.public() }

			socket.emit(
				'talkee-list',
				protochat.encode(proto.TalkeeList, talkee_list)
			)
		})

		socket.on('leave', () =>
		{
			if (! socket.chat)
			{
				return
			}

			var id = socket.chat.id
			var index = find(Aliases, (it) => it.id === id)

			if (~ index)
			{
				Aliases.splice(index, 1)

				var talkee = talkee_public(socket.chat)
				socket.broadcast.emit(
					'leaved',
					protochat.encode(proto.Talkee, talkee)
				)
			}
		})

		socket.on('send', ({ msg }) =>
		{
			if (! socket.chat)
			{
				return
			}

			var data =
			{
				talkee: talkee_public(socket.chat),
				msg,
			}

			socket.broadcast.emit('recv', protochat.encode(proto.Reply, data))
		})
	})
}
